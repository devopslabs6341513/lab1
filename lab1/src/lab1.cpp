#include <iostream>

// Функция для вычисления факториала числа n
unsigned long long factorial(int n) {
    if (n < 0) {
        std::cerr << "Ошибка: Факториал отрицательного числа не определен.\n";
        return 0; // Возвращаем 0 в случае ошибки
    }
    
    unsigned long long result = 1;
    for (int i = 1; i <= n; ++i) {
        result *= i;
        
        // Проверка на переполнение
        if (result <= 0) {
            std::cerr << "Ошибка: Вычисление факториала привело к переполнению.\n";
            return 0; // Возвращаем 0 в случае ошибки
        }
    }
    
    return result;
}

int main() {
    int n;
    std::cout << "Введите число для вычисления его факториала: ";
    std::cin >> n;
    
    // Проверка на корректность ввода
    if (std::cin.fail()) {
        std::cerr << "Ошибка: Некорректный ввод.\n";
        return 1; // Возвращаем код ошибки
    }
    
    // Вычисляем факториал и выводим результат
    unsigned long long result = factorial(n);
    if (result != 0) {
        std::cout << "Факториал числа " << n << " равен " << result << std::endl;
    }
    
    return 0;
}
