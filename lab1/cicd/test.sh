#!/bin/bash

# Функция для тестирования программы на вычисление факториала
test_factorial() {
    input=$1
    expected_output=$2
    
    # Запускаем программу и захватываем вывод
    output=$(./your_program $input)
    
    # Сравниваем вывод с ожидаемым результатом
    if [ "$output" -eq "$expected_output" ]; then
        echo "Тест для числа $input прошел успешно. Факториал: $output"
    else
        echo "Тест для числа $input не прошел. Ожидался результат: $expected_output, полученный результат: $output"
    fi
}

# Тестирование для нескольких чисел
test_factorial 5 120
test_factorial 10 3628800
test_factorial -5 0 # Проверка на отрицательное число
test_factorial 20 2432902008176640000

# Добавьте другие тесты по вашему усмотрению

echo "Все тесты завершены."